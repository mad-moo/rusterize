use rusterize;
use image;


fn draw_triangle(
        triangle: &rusterize::Triangle,
        target: &mut image::RgbImage,
        color: image::Rgb<u8>) {

    // Rasterize
    for frag in triangle.rasterize(target.width() as usize, target.height() as usize) {
        target[(frag.x as u32, frag.y as u32)] = color;
    }
}


fn main() {
    // Create the image to render into
    let mut img: image::RgbImage = image::ImageBuffer::new(1280, 720);

    // Draw a clockwise triangle
    let triangle = rusterize::Triangle {
        x1: 0.8, y1: 0.1,
        x2: 0.1, y2: 0.8,
        x3: 0.1, y3: 0.1,
    };

    assert!(triangle.is_clockwise());
    draw_triangle(&triangle, &mut img, image::Rgb([255, 0, 0]));

    // And a counter-clockwise one
    let triangle = rusterize::Triangle {
        x1: 0.9, y1: 0.2,
        x2: 0.2, y2: 0.9,
        x3: 0.9, y3: 0.9,
    };

    assert!(!triangle.is_clockwise());
    draw_triangle(&triangle, &mut img, image::Rgb([0, 255, 0]));

    // Write out the result
    img.save("winding_out.png").unwrap();
}
