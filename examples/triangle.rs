use rusterize;
use image;


fn main() {
    // Create the image to render into
    let mut img: image::RgbImage = image::ImageBuffer::new(1280, 720);

    // Set up a test triangle
    let scale: f32 = 1.0;

    let triangle = rusterize::Triangle {
        x1: 0.5 * scale, y1: 0.1 * scale,
        x2: 0.1 * scale, y2: 0.9 * scale,
        x3: 0.9 * scale, y3: 0.9 * scale,
    };

    // Rasterize
    for frag in triangle.rasterize(img.width() as usize, img.height() as usize) {
        // Calculate the pixel color
        let r: f32 = frag.b1;
        let g: f32 = frag.b2;
        let b: f32 = frag.b3;

        // Apply gamma
        let gamma: f32 = 1.0 / 2.2;
        let r = r.powf(gamma);
        let g = g.powf(gamma);
        let b = b.powf(gamma);

        // Write out the pixel value
        img[(frag.x as u32, frag.y as u32)] = image::Rgb([
            (r * 255f32) as u8,
            (g * 255f32) as u8,
            (b * 255f32) as u8,
        ]);
    }

    // Write out the result
    img.save("triangle_out.png").unwrap();
}
